
class Account {
    constructor(balance, currency) {
        this.balance = balance;
        this.currency = currency;
    }
};

class Person {
    constructor(firstName, lastName, accountsList) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.accountsList = accountsList;
    }

    addAccount(balance, currency) {
        this.accountsList.push(new Account(balance, currency));
    }

    _calculateBalance() {
        var total = 0;
        for (var value of this.accountsList) {
            total += value.balance;
        }
        return total;
    }

    sayHello() {
        return `First Name: ${this.firstName}, Last Name: ${this.lastName}, Total Balance: ${this._calculateBalance()}`;
    }

    filterPositiveAccounts() {
        return this.accountsList.filter((account) => account.balance > 0);
    }
};



const marlena = new Person('Marlena', 'Szwec', [new Account(100, 'dolar'), new Account(1, 'dolar')]);
console.log(marlena.sayHello());
marlena.addAccount(1, 'dolar');
console.log(marlena.sayHello());
console.log(marlena.filterPositiveAccounts());
marlena.addAccount(-1, 'dolar');
console.log(marlena.sayHello());
console.log(marlena.filterPositiveAccounts());




