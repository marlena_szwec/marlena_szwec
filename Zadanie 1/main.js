var personFactory = function () {
    var details = {
        firstName: 'Jean-Claude',
        lastName: 'van Damme',
        accountsList: [{
            balance: 1589,
            currency: 'dolar',
        }, {
            balance: 1589,
            currency: 'dolar',
        }]
    }

    return {
        firstName: details.firstName,
        lastName: details.lastName,
        
        sayHello: function () {
            return 'First Name: ' + details.firstName + ' ' + 'Last Name: ' + details.lastName + ' ' + 'Number of accounts: ' + details.accountsList.length + '.'
        }
    };
}

myObject = personFactory();
console.log(myObject.sayHello());




