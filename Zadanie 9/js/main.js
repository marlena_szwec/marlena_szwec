class Account {
    constructor(balance, currency, number) {
        this.balance = balance;
        this.currency = currency;
        this.number = number;
    }
};

class Person {
    constructor(firstName, lastName, accountsList) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.accountsList = accountsList;
    }

    addAccount(balance, currency, number) {
        this.accountsList.push(new Account(balance, currency, number));
    }

    _calculateBalance() {
        var total = 0;
        for (var value of this.accountsList) {
            total += value.balance;
        }
        return total;
    }

    findAccount(accountNumber) {
        return this.accountsList.find(account => (account.number === accountNumber));
    }

    sayHello() {
        return `First Name: ${this.firstName}, Last Name: ${this.lastName}, Total Balance: ${this._calculateBalance()}`;
    }

    filterPositiveAccounts() {
        return this.accountsList.filter(account => account.balance > 0);
    }

    withdraw(accountNumber, amount) {
            return new Promise((resolve, reject) => {
            let account = this.findAccount(accountNumber);
            setTimeout(function () {
                if (account && account.balance > amount) {
                    account.balance -= amount;
                    resolve(`Account Number: ${accountNumber} New Balance: ${account.balance} Amount of withdrawn money: ${amount}`);
                } else {
                    reject("The account can't be found or the requested amount is bigger than account balance.");
                }
            }, 3000)
        })
    }
};

const init = (() => {
    function onClicked() {
        const marlena = new Person('Marlena', 'Szwec', [new Account(100, 'dolar', 1), new Account(1, 'dolar', 2)]);
        document.querySelector('.btn.btn-primary').onclick = withdrawal;

        function withdrawal() {

            let accountNum = parseInt(document.getElementById('number').value);
            let amount = parseFloat(document.getElementById('amount').value);

            marlena.withdraw(accountNum, amount)
                .then(() => {
                    let accountNew = marlena.findAccount(accountNum);
                    let printNew = document.getElementById('account_' + accountNew.number);
                    printNew.textContent = `Balance: ${accountNew.balance} Currency: ${accountNew.currency} Account Number: ${accountNew.number}`;
                    alert(`Your transaction was successful! \nWithdrawal amount: ${amount} $ \nAccount balance: ${accountNew.balance} $`);
                    //var div = document.createElement("DIV");  
                    //var transactionInfo = document.createTextNode(`Your transaction was successful! \nWithdrawal amount: ${amount} $ \nAccount balance: ${accountNew.balance} $`);
                    //div.appendChild(transactionInfo);
                    //document.body.appendChild(div);
                })
                .catch((reject) => {
                    let message_alert = reject;
                    alert(message_alert);
                });
        };

        (function printFirstandLastName() {
            let printFirstName = document.querySelector(".card-title");
            let message_01 = `${marlena.firstName} ${marlena.lastName}`;
            printFirstName.innerHTML = message_01;
        })();

        (function printAccounts() {
            let printAccount = document.querySelector(".card-text");

            for (let i = 0; i < marlena.accountsList.length; i++) {
                printAccount.innerHTML += `<p id=account_${marlena.accountsList[i].number}> Balance: ${marlena.accountsList[i].balance} Currency: ${marlena.accountsList[i].currency} Account Number: ${marlena.accountsList[i].number}</p>`;
            }
        })();
    }

    function onChanged() {

        (function withdrawButton() {

            let withdrawButton = document.querySelector('.btn.btn-primary');
            withdrawButton.disabled = true;

            let accountNum = document.getElementById('number');
            let amount = document.getElementById('amount');

            function addListenerForActivation(field) {
                field.addEventListener('change', active);
                field.addEventListener('keyup', active);
            }
            addListenerForActivation(accountNum);
            addListenerForActivation(amount);

            function active() {
                withdrawButton.disabled = accountNum.value && amount.value ? false : true;
            };
        })();
    }

    
        onClicked();
        onChanged();
    
})();









