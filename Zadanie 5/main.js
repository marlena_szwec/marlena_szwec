
class Account {
    constructor(balance, currency, number) {
        this.balance = balance;
        this.currency = currency;
        this.number = number;
    }
};

class Person {
    constructor(firstName, lastName, accountsList) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.accountsList = [accountsList];
    }

    addAccount(balance, currency, number) {
        this.accountsList.push(new Account(balance, currency, number));
    }

    _calculateBalance() {
        var total = 0;
        for (var value of this.accountsList) {
            total += value.balance;
        }
        return total;
    }

    findAccount(accountNumber) {
        return this.accountsList.find(account => (account.number === accountNumber));
    }

    sayHello() {
        return `First Name: ${this.firstName}, Last Name: ${this.lastName}, Total Balance: ${this._calculateBalance()}`;
    }

    filterPositiveAccounts() {
        return this.accountsList.filter(account => account.balance > 0);
    }

    withdraw(accountNumber, amount) {
            return new Promise((resolve, reject) => {
            var account = this.findAccount(accountNumber);
            setTimeout(function () {
                if (account && account.balance > amount) {
                    account.balance -= amount;
                    resolve(`Account Number: ${accountNumber} New Balance: ${account.balance} Amount of withdrawn money: ${amount}`);
                } else {
                    reject("The account can't be found or the requested amount is bigger than account balance");
                }
            }, 3000)
        })
    }
};



const marlena = new Person('Marlena', 'Szwec', [new Account(100, 'dolar', 1), new Account(1, 'dolar', 2)]);
console.log(marlena.sayHello());
marlena.addAccount(1, 'dolar', 3);
console.log(marlena.sayHello());
console.log(marlena.filterPositiveAccounts());
marlena.addAccount(-1, 'dolar', 4);
console.log(marlena.sayHello());
console.log(marlena.filterPositiveAccounts());
console.log(marlena.findAccount(2));


marlena.withdraw(1, 1).then((resolve) => console.log(resolve)).catch((reject) => console.log(reject));
marlena.withdraw(1, 1000).then((resolve) => console.log(resolve)).catch((reject) => console.log(reject));
marlena.withdraw(5, 1).then((resolve) => console.log(resolve)).catch((reject) => console.log(reject));





