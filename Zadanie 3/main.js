var Account = function (balance, currency) {
    this.balance = balance;
    this.currency = currency;
};

var person = (function () {
    var details = {
        firstName: 'Jean-Claude',
        lastName: 'van Damme',
    }

    return {
        firstName: details.firstName,
        lastName: details.lastName,
        accountsList: [new Account(1589, 'dolar'),
        new Account(6000, 'dolar')],

        calculateBalance: function () {
            var total = 0;
            for (var i = 0; i < this.accountsList.length; i++) {
                total += this.accountsList[i].balance;
            }
            return total;
        },

        sayHello: function () {
            return 'First Name: ' + this.firstName + ' ' + 'Last Name: ' + this.lastName + ' ' + 'Total Balance: ' + this.calculateBalance() + '.'
        },

        addAccount: function (balance, currency) {
            this.accountsList.push(new Account(balance, currency));
        },
    }
})();


console.log(person.sayHello());
person.addAccount(1000, 'dolar');
console.log(person.sayHello());





