var person = (function () {
    var details = {
        firstName: 'Jean-Claude',
        lastName: 'van Damme',
    };

    return {
        firstName: details.firstName,
        lastName: details.lastName,
        accountsList: [{
            balance: 1589,
            currency: 'dolar',
        }, {
            balance: 6000,
            currency: 'dolar',
        }],

        calculateBalance: function () {
            var total = 0;
            for (var i = 0; i < this.accountsList.length; i++) {
                total += this.accountsList[i].balance;
            }
            return total;
        },

        sayHello: function () {
            return 'First Name: ' + this.firstName + ' ' + 'Last Name: ' + this.lastName + ' ' + 'Total Balance: ' + this.calculateBalance() + '.'
        },

        addAccount(balance, currency) {
            this.accountsList.push({ balance: balance, currency: currency });
        },
    }


})();

console.log(person.sayHello());
person.addAccount(6100, 'dolar');
console.log(person.sayHello());





